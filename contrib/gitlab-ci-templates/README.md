GitLab OBS CI Templates
=======================

These are some example GitLab CI templates for using `obsctl` in a pipeline.

## Using these templates

* Copy the contents of `v0/{type}-pipeline-example.yml` into your own project's `.gitlab-ci.yml`.
* Merge `stages` with your own stages, or change those used in this example.
* Set the `OBS_PROJECT` variable to the name of your OBS project.
* Ensure the `CI_TEMPLATES_IS_UPSTREAM` variable is set at the group level (in Settings: CI/CD: Variables, not in yml committed to the repo), so the jobs can identify upstream repositories.
* Add, update, and remove scratch build jobs to select appropriate distros.

## Files

* `v0/*-pipeline-example.yml` are examples of configuration to integrate for various package build types
* `v0/*-pipeline.yml` are configuration intended to be included by a project via the `include:` keyword.
* `v0/*-templates.yml` provide GitLab CI/CD configuration sections that are used by `v0/{type}-pipeline.yml` and your project's `.gitlab-ci.yml` to assemble jobs.

