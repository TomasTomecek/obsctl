OBSCtl container
================

This is an example container for running `obsctl` in a CI system.

Provided is an example `Containerfile` that can be built with [Podman](https://podman.io) or Docker.
Additionally, an example `OpenShift.yaml` that can build and publish the image in [Red Hat OpenShift](https://www.redhat.com/en/technologies/cloud-computing/openshift) for use in cloud-native CI/CD systems is provided.

## Building the container with Podman/Docker

The steps below are done with Podman, though you should be able to substitute with Docker easily enough.

```
$ podman build -f Containerfile -t obsctl --build-arg=OBS_SERVER=build.example.org --build-arg=OBS_USER=nobody --build-arg=OBS_PASS=nopass
```

Obviously, substitute `OBS_SERVER`, `OBS_USER`, and `OBS_PASS` for what you want to configure with.

Then you can freely use the container locally with `podman run --rm -it obsctl` or whatever way you'd like.

## Building the container in OpenShift

This requires at least Red Hat OpenShift Container Platform or [OKD](https://okd.io) 4.1 or newer.
OpenShift/OKD 3.11 support requires downgrading the container to CentOS Stream 8 due to seccomp
issues caused by RHEL/CentOS 7-based Docker used in OpenShift/OKD 3.11.

Assuming you're using OpenShift/OKD 4.x and you've already installed the `oc(1)` tool, building
it is easy enough.

First, create `OpenShift.yaml.configured` based on the `OpenShift.yaml` file included and set `OBS_SERVER`, `OBS_USER`, and `OBS_PASS`
to the correct information for your setup.

Then, import and build the image (assuming your OpenShift project/namespace is `build-containers`):

```
$ oc create -f OpenShift.yaml.configured -n build-containers
$ oc start-build obsctl -n build-containers
```

Now you have the image built in your environment that you can use inside your cluster.
