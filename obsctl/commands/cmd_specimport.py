# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module implementing command to submit flat spec packaging (spec + sources) to OBS"""

from __future__ import print_function

import sys
import pathlib

import click

from obsctl.cli import PASS_CONTEXT
from obsctl.lib import distgit
from obsctl.lib import obsc


@click.command("specimport", help="Push packaging spec sources to an OBS instance.")
@click.option(
    "--pkg-scm-source",
    "-S",
    help="Package SCM source distribution",
    type=click.Choice(["fedora"]),
    default="fedora",
)
@click.option(
    "--repo-type",
    "-T",
    help="Source repository type",
    type=click.Choice(["git", "hg", "svn"]),
    required=True,
)
@click.option("--repo-owner", "-o", help="Source repository owner", required=True)
@click.option("--repo-name", "-n", help="Source repository name", required=True)
@click.option("--repo-reltag", "-t", help="Source repository release tag", required=True)
@click.option("--obs-project", "-s", help="Open Build Service project space", required=True)
@click.option("--obs-package", "-p", help="Open Build Service project package", required=True)
@click.option(
    "--create-obs-pkg-if-none",
    "-c",
    help="Create package space in OBS if it doesn't exist",
    is_flag=True,
    default=False,
)
@click.option(
    "--use-default-service-file-if-none",
    "-d",
    help="Generate the default download-url service file if no _service file exists",
    is_flag=True,
    default=False,
)
@click.option(
    "--insecure", "-i", help="Ignore issues with HTTPS (self signed certs, etc.)", is_flag=True
)
@PASS_CONTEXT
def cli(
    ctx,
    pkg_scm_source,
    repo_type,
    repo_owner,
    repo_name,
    repo_reltag,
    obs_project,
    obs_package,
    create_obs_pkg_if_none,
    use_default_service_file_if_none,
    insecure,
):
    # Disable linting because the click framework expects to pass arguments. The invalid name is
    # because use_default_service_file_if_none is too long, but we don't control that.
    # pylint: disable=too-many-arguments,too-many-locals,invalid-name
    """Import spec packaging (spec + sources) into OBS"""

    # TODO: Clean this up
    args = {
        "pkg_scm_source": pkg_scm_source,
        "src_repo_type": repo_type,
        "src_repo_owner": repo_owner,
        "src_repo_name": repo_name,
        "src_repo_tag": repo_reltag,
        "obs_server_addr": ctx.obs_server,
        "obs_auth_user": ctx.obs_user,
        "obs_auth_pass": ctx.obs_pass,
        "obs_project": obs_project,
        "obs_package": obs_package,
        "create_obs_pkg_if_none": create_obs_pkg_if_none,
        "use_default_service_file_if_none": use_default_service_file_if_none,
        "insecure": insecure,
    }

    # Configure the Open Build Service Commander
    obsc.configure(
        args["obs_server_addr"], args["obs_auth_user"], args["obs_auth_pass"], args["insecure"]
    )

    obspkg = obsc.checkout_obs_package(
        args["obs_server_addr"],
        args["obs_project"],
        args["obs_package"],
        args["obs_auth_user"],
        args["src_repo_owner"],
        args["src_repo_name"],
        args["create_obs_pkg_if_none"],
    )

    if obspkg is False:
        print("No package space exists, exiting!")
        sys.exit(2)

    if pathlib.Path("./sources").is_file():
        print("Dist-Git 'sources' file detected, fetching files from Dist-Git...")
        distgit.download_sources(
            args["obs_package"], str(pathlib.Path(".").resolve()), args["pkg_scm_source"]
        )

    # Enumerate all source files
    srcfiles = [
        str(src.resolve())
        for src in pathlib.Path(".").iterdir()
        if src.resolve().is_file()
        and not src.name == "sources"
        and not src.name == "_service"
        and not src.name.startswith(".")
    ]

    # Default service file for retrieving sources using spec
    obs_package_service = """
    <services>
        <service name="download_files">
        <param name="enforceupstream">yes</param>
       </service>
    </services>
    """

    # If specified, write service file out if one doesn't exist
    if not pathlib.Path("./_service").is_file() and not pathlib.Path("./sources").is_file():
        if args["use_default_service_if_none"] is True:
            obs_package_service_file = open(
                "{0}/_service".format(str(pathlib.Path(".").resolve())), "w"
            )
            obs_package_service_file.write(obs_package_service.encode("utf-8"))
            obs_package_service_file.close()
        else:
            print("No _service file exists in the repository!")
            sys.exit(6)

    # Send changes to OBS
    if pathlib.Path("./sources").is_file():
        obsc.update_sources_for_obs_package(obspkg, args["src_repo_tag"], srcfiles)
    else:
        obsc.update_files_for_obs_package(
            obspkg, args["src_repo_tag"], srcfiles, str(pathlib.Path("./_service").resolve())
        )

    # Print out where people can see the release
    print(
        "The submitted build can be observed at: {obssrv}/package/show/{obsprj}/{obspkg}".format(
            obssrv=args["obs_server_addr"], obsprj=args["obs_project"], obspkg=args["obs_package"]
        )
    )
