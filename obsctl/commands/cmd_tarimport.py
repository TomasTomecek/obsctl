# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module implementing command to submit tarball with embedded spec to OBS"""

from __future__ import print_function

import os
import shutil
import sys
import tempfile

import click

from obsctl.cli import PASS_CONTEXT
from obsctl.lib import obsc
from obsctl.lib import spec
from obsctl.lib import tar
from obsctl.lib import util


@click.command("tarimport", help="Push tarball of sources to an OBS instance.")
@click.option("--source-file", "-f", help="Path to source tarball file", required=True)
@click.option(
    "--extract-spec-dir",
    "-x",
    default="dist/",
    help="Path relative to source root where to extract spec file",
)
@click.option(
    "--extract-extra-spec-files",
    "-e",
    help="Extra spec files to extract from spec dir",
    multiple=True,
)
@click.option("--continuous-merge", "-m", help="Enable continuous merge mode", is_flag=True)
@click.option(
    "--repo-type",
    "-T",
    help="Source repository type",
    type=click.Choice(["git", "hg", "svn"]),
    required=True,
)
@click.option("--repo-owner", "-o", help="Source repository owner", required=True)
@click.option("--repo-name", "-n", help="Source repository name", required=True)
@click.option("--repo-reltag", "-t", help="Source repository release tag", required=True)
@click.option("--repo-revision", "-R", help="Source repository revision/commit", required=True)
@click.option("--repo-branch", "-B", help="Source repository branch", required=True)
@click.option(
    "--revision-form",
    "-r",
    help="Form of version for spec",
    type=click.Choice(["prerel", "postrel"]),
    default="postrel",
)
@click.option("--obs-project", "-s", help="Open Build Service project space", required=True)
@click.option("--obs-package", "-p", help="Open Build Service project package", required=True)
@click.option(
    "--create-obs-pkg-if-none",
    "-c",
    help="Create package space in OBS if it doesn't exist",
    is_flag=True,
)
@click.option(
    "--add-download-files-service",
    "-d",
    help="Add download-file source service to generated service file",
    is_flag=True,
)
@click.option(
    "--insecure", "-i", help="Ignore issues with HTTPS (self signed certs, etc.)", is_flag=True
)
@click.option("--watch-build", "-w", help="Watch the build log for the specified repository/arch")
@PASS_CONTEXT
def cli(
    ctx,
    source_file,
    extract_spec_dir,
    extract_extra_spec_files,
    continuous_merge,
    repo_type,
    repo_owner,
    repo_name,
    repo_reltag,
    repo_revision,
    repo_branch,
    revision_form,
    obs_project,
    obs_package,
    create_obs_pkg_if_none,
    add_download_files_service,
    insecure,
    watch_build,
):
    # Disable linting because the click framework expects to pass arguments.
    # pylint: disable=too-many-arguments,too-many-locals,invalid-name,too-many-branches,too-many-statements
    """Import tarball with embedded spec to OBS"""

    # TODO: Clean this up
    args = {
        "src_tarball": source_file,
        "specdir_path": extract_spec_dir,
        "spec_extras": extract_extra_spec_files,
        "ci_mode": continuous_merge,
        "src_repo_type": repo_type,
        "src_repo_owner": repo_owner,
        "src_repo_name": repo_name,
        "src_repo_tag": repo_reltag,
        "src_repo_rev": repo_revision,
        "src_repo_branch": repo_branch,
        "spec_rev_form": revision_form,
        "obs_server_addr": ctx.obs_server,
        "obs_auth_user": ctx.obs_user,
        "obs_auth_pass": ctx.obs_pass,
        "obs_project": obs_project,
        "obs_package": obs_package,
        "create_obs_pkg_if_none": create_obs_pkg_if_none,
        "add_download_files_service": add_download_files_service,
        "insecure": insecure,
        "watch_build": watch_build,
    }

    # Configure the Open Build Service Commander
    obsc.configure(
        args["obs_server_addr"], args["obs_auth_user"], args["obs_auth_pass"], args["insecure"]
    )

    # validate the watch_build parameter
    distribution, architecture = None, None
    if args["watch_build"] is not None:
        if "/" not in args["watch_build"]:
            print(
                "The repo and arch must be specified for the --watch-build parameter. E.G. xUbuntu_20.04/x86_64",
                file=sys.stderr,
            )
            sys.exit(1)
        distribution, architecture = args["watch_build"].split("/")
        project_repos = obsc.get_project_repos(args["obs_server_addr"], args["obs_project"])
        matched_repo = [
            repo
            for repo in project_repos
            if repo.name == distribution and repo.arch == architecture
        ]
        if not matched_repo:
            print(
                f"The OBS project {args['obs_project']} is not built for {distribution}/{architecture}.",
                file=sys.stderr,
            )
            print(
                "The following distributions and architectures are supported in this project:",
                file=sys.stderr,
            )
            for project_repo in project_repos:
                print(f" • {project_repo.name}/{project_repo.arch}", file=sys.stderr)
            sys.exit(1)

    obspkg = obsc.checkout_obs_package(
        args["obs_server_addr"],
        args["obs_project"],
        args["obs_package"],
        args["obs_auth_user"],
        args["src_repo_owner"],
        args["src_repo_name"],
        args["create_obs_pkg_if_none"],
    )

    if obspkg is False:
        print("No package space exists, exiting!")
        sys.exit(2)

    if args["ci_mode"] is True:
        # Generate a snapshot version
        release_tarball_ver = spec.generate_snapshot_version(
            args["src_repo_tag"],
            args["src_repo_type"],
            args["src_repo_rev"],
            args["src_repo_branch"],
            args["spec_rev_form"],
        )
    else:
        release_tarball_ver = args["src_repo_tag"]

    rebuilt_tarball_name = tar.rebuild_tarball(
        args["src_tarball"], args["src_repo_name"], release_tarball_ver
    )

    print("{0} has been rebuilt.".format(rebuilt_tarball_name))

    # Auto-set the version
    if args["ci_mode"] is True:
        obs_version_service_chunk = """
      <service name="set_version">
        <param name="version">{0}</param>
        <param name="file">_service:extract_file:{1}.spec</param>
      </service>""".format(
            release_tarball_ver, args["obs_package"]
        )
    else:
        obs_version_service_chunk = ""

    # Add download files service if requested
    if args["add_download_files_service"] is True:
        obs_download_files_service_chunk = """
      <service name="download_files">
        <param name="enforceupstream">yes</param>
      </service>"""
    else:
        obs_download_files_service_chunk = ""

    specdir_path = args["specdir_path"].strip("/")

    if args["spec_extras"]:
        obs_extract_extra_files_chunk = ""
        for spec_extra_file in args["spec_extras"]:
            obs_extract_extra_files_chunk += """
        <param name="files">*{0}*/{1}/{2}</param>
        """.format(
                args["src_repo_name"], specdir_path, spec_extra_file
            )
    else:
        obs_extract_extra_files_chunk = ""

    # Service file for retrieving the spec from the tarball
    obs_package_service = """
    <services>
      <service name="extract_file">
        <param name="archive">{0}*.tar.gz</param>
        <param name="files">*{0}*/{1}/{2}.spec</param>{3}
      </service>{4}{5}
    </services>
    """.format(
        args["src_repo_name"],
        specdir_path,
        args["obs_package"],
        obs_extract_extra_files_chunk,
        obs_version_service_chunk,
        obs_download_files_service_chunk,
    )

    # Check if the spec file version matches what we're targeting (if not CI mode)
    if args["ci_mode"] is not True:
        full_relative_spec_path = "./{0}/{1}.spec".format(specdir_path, args["obs_package"])
        spec_vermatch = spec.validate_version_match(full_relative_spec_path, args["src_repo_tag"])
        if not spec_vermatch:
            print("ERROR: The spec file version does not match the repo tag!")
            sys.exit(15)

    # Write service file out

    obs_package_service_file = tempfile.NamedTemporaryFile(suffix="_obssrcservice")

    obs_package_service_file.write(obs_package_service.encode("utf-8"))
    obs_package_service_file.flush()

    obs_package_metadata = [args["obs_server_addr"], args["obs_project"], args["obs_package"]]
    obs_build_metadata = [distribution, architecture]

    pre_upload_job_id = None
    if args["watch_build"]:
        pre_upload_job_id = obsc.get_package_build_jobid(*obs_package_metadata, *obs_build_metadata)

    # Send changes to OBS
    obsc.update_files_for_obs_package(
        obspkg, release_tarball_ver, [rebuilt_tarball_name], obs_package_service_file.name
    )

    # Purge working things
    shutil.rmtree(os.path.dirname(rebuilt_tarball_name))
    obs_package_service_file.close()

    if args["watch_build"]:

        def detect_build_start():
            """check for code="building" and a different jobid than we started with"""
            package_state = obsc.get_package_result_state(
                *obs_package_metadata, *obs_build_metadata
            )
            if package_state == "building":
                post_upload_job_id = obsc.get_package_build_jobid(
                    *obs_package_metadata, *obs_build_metadata
                )
                return post_upload_job_id is not None and post_upload_job_id != pre_upload_job_id
            return False

        def build_log_has_content():
            """check if OBS has started writing to the log"""
            log_snippet = obsc.get_log_snippet(*obs_package_metadata, *obs_build_metadata, length=1)
            return len(log_snippet) == 1

        def check_build_result(ok_codes, ok_states):
            package_result = obsc.get_package_results(*obs_package_metadata, *obs_build_metadata)
            code = package_result[0]["code"]
            state = package_result[0]["state"]
            return code in ok_codes and state in ok_states

        def build_finished():
            return check_build_result(("succeeded", "failed"), "published")

        def build_succeeded():
            return check_build_result("succeeded", "published")

        print("Waiting for the build to start.")

        try:
            util.wait_for_task(detect_build_start)
            util.wait_for_task(build_log_has_content)
        except TimeoutError:
            print("Timed out while waiting for the build to start.", file=sys.stderr)
            sys.exit(3)

        print("The build has started. Monitoring output...")

        obsc.print_buildlog(*obs_package_metadata, *obs_build_metadata)

        try:
            util.wait_for_task(build_finished)
        except TimeoutError:
            print(
                "Timed out while waiting for the package status to indicate build completion.",
                file=sys.stderr,
            )
            sys.exit(3)

        build_success = build_succeeded()
        print(f"\nBuild completed {'successfully' if build_success else 'unsuccessfully'}.")
        if not build_success:
            sys.exit(20)
    else:
        # Print out where people can see the release
        print(
            "The submitted build can be observed at: {obssrv}/package/show/{obsprj}/{obspkg}".format(
                obssrv=args["obs_server_addr"],
                obsprj=args["obs_project"],
                obspkg=args["obs_package"],
            )
        )
