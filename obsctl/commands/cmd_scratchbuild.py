# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module implementing the package scratch build command"""

from __future__ import print_function

import os
import shutil
import subprocess
import sys
import tempfile
import time

import click

from obsctl.lib import distgit
from obsctl.lib import obsc
from obsctl.lib import spec
from obsctl.lib import tar
from obsctl.lib import obssvc
from obsctl.cli import PASS_CONTEXT


@click.command(
    "scratchbuild",
    help="Executes a local scratch build for OBS projects " "given a tarball",
    short_help="Executes local OBS scratch build",
)
@click.option("--source-file", "-f", help="Path to source tarball file", required=True)
@click.option(
    "--extract-spec-dir",
    "-x",
    default="dist/",
    help="Path relative to source root where to extract spec file",
)
@click.option(
    "--extract-extra-spec-files",
    "-e",
    help="Extra spec files to extract from spec dir",
    multiple=True,
)
@click.option("--pkg-scm", "-P", help="Switch to Package SCM mode", is_flag=True)
@click.option(
    "--pkg-scm-source",
    "-S",
    help="Package SCM source distribution",
    type=click.Choice(["fedora"]),
    default="fedora",
)
@click.option("--continuous-merge", "-m", help="Enable continuous merge mode", is_flag=True)
@click.option(
    "--repo-type",
    "-T",
    help="Source repository type",
    type=click.Choice(["git", "hg", "svn"]),
    default="git",
)
@click.option("--repo-owner", "-o", help="Source repository owner", required=True)
@click.option("--repo-name", "-n", help="Source repository name", required=True)
@click.option("--repo-reltag", "-t", help="Source repository release tag", required=True)
@click.option("--repo-revision", "-R", help="Source repository revision/commit", required=True)
@click.option("--repo-branch", "-B", help="Source repository branch", required=True)
@click.option(
    "--revision-form",
    "-r",
    help="Form of version for spec",
    type=click.Choice(["prerel", "postrel", "prerel-upver"]),
    default="postrel",
)
@click.option("--obs-project", "-s", help="Open Build Service project space", required=True)
@click.option("--obs-package", "-p", help="Open Build Service project package", required=True)
@click.option("--build-distribution", "-d", required=True, help="Target distribution to build for")
@click.option(
    "--build-architecture", "-a", default="x86_64", help="Target architecture to build for"
)
@click.option("--get-external-sources", "-g", help="Fetch external sources", is_flag=True)
@click.option(
    "--insecure", "-i", help="Ignore issues with HTTPS (self signed certs, etc.)", is_flag=True
)
@click.option("with_features", "--with", help="Enable feature X for build", multiple=True)
@click.option("without_features", "--without", help="Disable feature X for build", multiple=True)
@PASS_CONTEXT
def cli(
    ctx,
    source_file,
    extract_spec_dir,
    extract_extra_spec_files,
    pkg_scm,
    pkg_scm_source,
    continuous_merge,
    repo_type,
    repo_owner,
    repo_name,
    repo_reltag,
    repo_revision,
    repo_branch,
    revision_form,
    obs_project,
    obs_package,
    build_distribution,
    build_architecture,
    get_external_sources,
    insecure,
    with_features,
    without_features,
):
    # Disable linting because the click framework expects to pass arguments.
    # pylint: disable=too-many-arguments,too-many-locals
    """Build the package from scratch locally using osc build."""

    # TODO: Simplify this, there must be some way to capture all arguments in
    #       a map or split things out more logically, this is for the sake of
    #       time.
    args = {
        "src_tarball": source_file,
        "specdir_path": extract_spec_dir,
        "spec_extras": extract_extra_spec_files,
        "pkg_scm_mode": pkg_scm,
        "pkg_scm_source": pkg_scm_source,
        "ci_mode": continuous_merge,
        "obs_server_addr": ctx.obs_server,
        "obs_auth_user": ctx.obs_user,
        "obs_auth_pass": ctx.obs_pass,
        "src_repo_type": repo_type,
        "src_repo_owner": repo_owner,
        "src_repo_name": repo_name,
        "src_repo_tag": repo_reltag,
        "src_repo_rev": repo_revision,
        "src_repo_branch": repo_branch,
        "spec_rev_form": revision_form,
        "obs_project": obs_project,
        "obs_package": obs_package,
        "build_dist": build_distribution,
        "build_arch": build_architecture,
        "get_external_srcs": get_external_sources,
        "insecure": insecure,
        "commit_time": time.strftime("%Y%m%d%H%M%S"),
        "with_features": with_features,
        "without_features": without_features,
    }
    main(args)


def execute(cmd, path):
    """Execute the subprocess."""
    curr_path = os.getcwd()
    os.chdir(path)
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True, shell=True)
    for stdout_line in iter(popen.stdout.readline, ""):
        yield stdout_line
    popen.stdout.close()
    os.chdir(curr_path)
    return_code = popen.wait()

    if return_code:
        print("Build command failed with exit code {}:".format(return_code), file=sys.stderr)
        print("Build command: {}".format(cmd), file=sys.stderr)
        print("Scratch build failed!")
        sys.exit(return_code)


def main(args):
    """Run the scratch build."""
    # Disable linting because the logic is simpler this way
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements

    oscconf = obsc.configure(
        args["obs_server_addr"],
        args["obs_auth_user"],
        args["obs_auth_pass"],
        insecure_connection=True,
    )

    spec_snap_version = spec.generate_snapshot_version(
        args["src_repo_tag"],
        args["src_repo_type"],
        args["src_repo_rev"],
        args["src_repo_branch"],
        args["spec_rev_form"],
    )

    obs_package_dir = tempfile.mkdtemp(
        suffix="{0}-{1}".format(args["obs_project"], args["obs_package"])
    )

    if args["pkg_scm_mode"] is True:
        # Package SCM mode forces certain behaviors off
        if args["ci_mode"] is True:
            print("Package SCM mode activated, continuous merge mode ignored!")
        if args["get_external_srcs"] is True:
            print("Package SCM mode activated, extra source service features ignored!")

        # Extract Package SCM contents to the temp directory
        tar.extract_tarball(args["src_tarball"], obs_package_dir)

        # If Package SCM contains Dist-Git 'sources' file...
        if os.path.exists("{0}/sources".format(obs_package_dir)):
            print("Dist-Git 'sources' file detected, fetching files from Dist-Git...")
            distgit.download_sources(args["obs_package"], obs_package_dir, args["pkg_scm_source"])
        # If Package SCM contains OBS source service file...
        elif os.path.exists("{0}/_service".format(obs_package_dir)):
            print("OBS source service declaration detected, running source services...")
            obs_pkgscm_service_file = open("{0}/_service".format(obs_package_dir), "r")
            obs_pkgscm_service = obs_pkgscm_service_file.read()
            obs_pkgscm_service_file.close()
            obssvc.run_source_services(obs_pkgscm_service, obs_package_dir)
    else:
        if args["ci_mode"] is True:
            release_tarball_ver = spec_snap_version
        else:
            release_tarball_ver = args["src_repo_tag"]

        rebuilt_tarball = tar.rebuild_tarball(
            args["src_tarball"], args["src_repo_name"], release_tarball_ver
        )

        shutil.move(rebuilt_tarball, obs_package_dir)
        shutil.rmtree(os.path.dirname(rebuilt_tarball))

        if args["get_external_srcs"] is True:
            obs_download_files_service = """
        <service name="download_files">
            <param name="enforceupstream">yes</param>
        </service>
            """
        else:
            obs_download_files_service = ""

        specdir_path = args["specdir_path"].strip("/")

        if args["spec_extras"]:
            obs_extract_extra_files = ""
            for spec_extra_file in args["spec_extras"]:
                obs_extract_extra_files += """
            <param name="files">*{srcrepo}*/{specpath}/{spec_extra}</param>
            """.format(
                    srcrepo=args["src_repo_name"], specpath=specdir_path, spec_extra=spec_extra_file
                )
        else:
            obs_extract_extra_files = ""

        obs_package_service = """
        <services>
        <service name="extract_file">
            <param name="archive">{srcrepo}-{scratchversion}.tar.gz</param>
            <param name="files">*{srcrepo}*/{specpath}/{package}.spec</param>{extrafiles}
        </service>
        <service name="set_version">
            <param name="version">{scratchversion}</param>
            <param name="file">_service:extract_file:{package}.spec</param>
        </service>{downloadsrc}
        </services>
        """.format(
            srcrepo=args["src_repo_name"],
            package=args["obs_package"],
            specpath=specdir_path,
            scratchversion=release_tarball_ver,
            downloadsrc=obs_download_files_service,
            extrafiles=obs_extract_extra_files,
        )

        # Add source service execution here
        obssvc.run_source_services(obs_package_service, obs_package_dir)

    # If feature flags were passed in, prepare them to be passed in
    with_args = " ".join(["--with " + feature for feature in args["with_features"]])
    without_args = " ".join(["--without " + feature for feature in args["without_features"]])

    # Prepare a "working directory"
    if not os.path.isdir(os.path.join(obs_package_dir, ".osc")):
        os.makedirs(os.path.join(obs_package_dir, ".osc"))

    if not os.path.isfile(os.path.join(obs_package_dir, ".osc", "_project")):
        prjfile = open(os.path.join(obs_package_dir, ".osc", "_project"), "w")
        prjfile.write(args["obs_project"])
        prjfile.flush()
        prjfile.close()

    if args["pkg_scm_mode"] is True:
        osc_build_spec_file = "{pkg}.spec".format(pkg=args["obs_package"])
    else:
        osc_build_spec_file = "_service:set_version:_service:extract_file:{pkg}.spec".format(
            pkg=args["obs_package"]
        )

    # TODO: Attempt to trigger builds through the osc API
    build_command = (
        "osc --config={conf} build "
        "--jobs=4 --local-package "
        "--alternative-project={obsprj} "
        "--trust-all-projects "
        "--no-verify --noservice {with_args} {without_args} "
        "--root={path}/scratchbuild-{dist}-{arch} {dist} {arch} "
        "{spec}".format(
            conf=oscconf.name,
            obsprj=args["obs_project"],
            path=os.getcwd(),
            dist=args["build_dist"],
            arch=args["build_arch"],
            spec=osc_build_spec_file,
            with_args=with_args,
            without_args=without_args,
        )
    )
    for path in execute(build_command, obs_package_dir):
        print(path, end="")

    # Clean up
    shutil.rmtree(obs_package_dir)
    oscconf.close()

    print("Scratch build completed successfully!")
