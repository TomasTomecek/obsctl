# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module implementing the package download command"""

import os
import osc.core

import click

from obsctl.cli import PASS_CONTEXT
from obsctl.lib import obsc


@click.command("getbinaries", help="Downloads binaries to a local directory")
@click.option("--sources", help="Also download source packages", is_flag=True)
@click.option("--debug", help="Also download debug packages", is_flag=True)
@click.option(
    "--destdir", "-d", default="./", type=click.Path(exists=True), help="Destination directory"
)
@click.option("--obs-project", "-s", help="Open Build Service project space", required=True)
@click.option("--obs-package", "-p", help="Open Build Service project package", required=True)
@click.option("--repo-name", "-n", help="Source repository name", required=True)
@click.option(
    "--repo-architecture", "-a", default="x86_64", help="Target repository binary architecture"
)
@PASS_CONTEXT
def cli(ctx, destdir, sources, debug, obs_project, obs_package, repo_name, repo_architecture):
    # Disable linting because the click framework expects to pass arguments.
    # pylint: disable=too-many-arguments,too-many-locals,invalid-name
    """Download packages from OBS"""

    obsc.configure(ctx.obs_server, ctx.obs_user, ctx.obs_pass, insecure_connection=True)

    binaries = osc.core.get_binarylist(
        ctx.obs_server, obs_project, repo_name, repo_architecture, package=obs_package
    )

    for f in binaries:
        if not (f.endswith("rpm") or f.endswith("deb")):
            continue
        if not sources and (f.endswith("src.rpm") or f.endswith("sdeb")):
            continue
        if not debug and (
            f.find("-debuginfo-") >= 0
            or f.find("-debugsource-") >= 0
            or f.find("-dbg") >= 0
            or f.find("-dbgsym") >= 0
        ):
            continue

        print("Downloading {0}".format(f))

        osc.core.get_binary_file(
            ctx.obs_server,
            obs_project,
            repo_name,
            repo_architecture,
            f,
            package=obs_package,
            target_filename=os.path.join(destdir, f),
        )
