# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module implementing command to submit dsc-style packaging to OBS"""

from __future__ import print_function

import sys
import pathlib
import glob
import shutil
import os

import click

from obsctl.cli import PASS_CONTEXT
from obsctl.lib import obsc
from obsctl.lib import dsc
from obsctl.lib import obssvc
from obsctl.lib import tar


@click.command("dscimport", help="Push packaging dsc sources to an OBS instance.")
@click.option(
    "--repo-type",
    "-T",
    help="Source repository type",
    type=click.Choice(["git", "hg", "svn"]),
    required=True,
)
@click.option("--repo-owner", "-o", help="Source repository owner", required=True)
@click.option("--repo-name", "-n", help="Source repository name", required=True)
@click.option("--repo-reltag", "-t", help="Source repository release tag", required=True)
@click.option("--obs-project", "-s", help="Open Build Service project space", required=True)
@click.option("--obs-package", "-p", help="Open Build Service project package", required=True)
@click.option(
    "--create-obs-pkg-if-none",
    "-c",
    help="Create package space in OBS if it doesn't exist",
    is_flag=True,
    default=False,
)
@click.option(
    "--insecure", "-i", help="Ignore issues with HTTPS (self signed certs, etc.)", is_flag=True
)
@PASS_CONTEXT
def cli(
    ctx,
    repo_type,
    repo_owner,
    repo_name,
    repo_reltag,
    obs_project,
    obs_package,
    create_obs_pkg_if_none,
    insecure,
):
    # Disable linting because the click framework expects to pass arguments.
    # pylint: disable=too-many-arguments,too-many-locals
    """Import dsc-style Debian packaging into OBS"""
    # TODO: Clean this up
    args = {
        "src_repo_type": repo_type,
        "src_repo_owner": repo_owner,
        "src_repo_name": repo_name,
        "src_repo_tag": repo_reltag,
        "obs_server_addr": ctx.obs_server,
        "obs_auth_user": ctx.obs_user,
        "obs_auth_pass": ctx.obs_pass,
        "obs_project": obs_project,
        "obs_package": obs_package,
        "create_obs_pkg_if_none": create_obs_pkg_if_none,
        "insecure": insecure,
    }

    # Configure the Open Build Service Commander
    obsc.configure(
        args["obs_server_addr"], args["obs_auth_user"], args["obs_auth_pass"], args["insecure"]
    )

    obspkg = obsc.checkout_obs_package(
        args["obs_server_addr"],
        args["obs_project"],
        args["obs_package"],
        args["obs_auth_user"],
        args["src_repo_owner"],
        args["src_repo_name"],
        args["create_obs_pkg_if_none"],
    )

    if obspkg is False:
        print("No package space exists, exiting!")
        sys.exit(2)

    # Bomb out if a _service file doesn't exist
    if not pathlib.Path("./_service").resolve().is_file():
        print("No _service file exists in the repository!")
        sys.exit(6)

    if os.path.exists(os.path.join(obspkg.absdir, "_service")):
        # Merge our service snippet into the OBS package one
        obssvc.Service(obspkg.absdir + "/_service").init().merge("_service").write()

        # Copy it back to our location so that it gets picked up later
        os.unlink("./_service")
        shutil.copy2(obspkg.absdir + "/_service", "./_service")

    # Generate the debian source control data files
    for dsc_filename in glob.glob("*.dsc"):
        debsrc = dsc.Dsc(dsc_filename).init()
        tar_filename = debsrc.get_debian_source_filename()
        tar.create_debian_tarball(tar_filename)
        debsrc.update_sha_checksums().strip_gpg_headers().write()

    # Enumerate all source files
    srcfiles = [
        str(src.resolve())
        for src in pathlib.Path(".").iterdir()
        if src.resolve().is_file() and not src.name == "_service" and not src.name.startswith(".")
    ]

    # Send changes to OBS
    obsc.update_files_for_obs_package(
        obspkg, args["src_repo_tag"], srcfiles, str(pathlib.Path("./_service").resolve()), True
    )

    # Print out where people can see the release
    print(
        "The submitted build can be observed at: {obssrv}/package/show/{obsprj}/{obspkg}".format(
            obssrv=args["obs_server_addr"], obsprj=args["obs_project"], obspkg=args["obs_package"]
        )
    )
