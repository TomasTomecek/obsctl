# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module for initializing the global CLI context."""

import json
import os
import sys

import click


class Context:
    """Configuration context for click commands."""

    OBSAUTH_CONFIG_FILE = "/etc/obsctl/obsauth.json"

    def __init__(self):
        if os.path.isfile(self.OBSAUTH_CONFIG_FILE):
            self.load_configuration()
        else:
            self.prompt_for_configuration()

    def load_configuration(self):
        """Load configuration from config file."""
        try:
            with open((self.OBSAUTH_CONFIG_FILE), "r") as obsauth_file:
                obsauth_filedata = obsauth_file.read()
                obsauth_data = json.loads(obsauth_filedata)
                obsauth_file.close()
                self.obs_server = obsauth_data.get("obs_server")
                self.obs_user = obsauth_data.get("obs_user")
                self.obs_pass = obsauth_data.get("obs_pass")
        # We don't want to let any exception slip pass this error message
        # pylint: disable=broad-except
        except Exception as error:
            print("Error opening configuration file: {}".format(error))

    def prompt_for_configuration(self):
        """Prompt user for configuration."""
        obs_server = click.prompt("OBS server", default="https://api.opensuse.org")
        obs_user = click.prompt("OBS login user", type=str)
        obs_pass = click.prompt("OBS login password", type=str, hide_input=True)
        self.obs_server = obs_server
        self.obs_user = obs_user
        self.obs_pass = obs_pass

    def set_configuration(self):
        """Set the configuration."""
        click.edit(filename=self.OBSAUTH_CONFIG_FILE)
        # Now pull the proper configuration
        self.load_configuration()


PASS_CONTEXT = click.make_pass_decorator(Context, ensure=True)


class CLI(click.MultiCommand):
    """Autoload commands from the commands path."""

    def list_commands(self, ctx):
        """List the commands"""
        commands = []
        cmd_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), "commands"))
        for filename in os.listdir(cmd_folder):
            if filename.endswith(".py") and filename.startswith("cmd_"):
                commands.append(filename[4:-3])
        commands.sort()
        return commands

    # This is documented usage for multicommand in click
    # pylint: disable=arguments-differ
    def get_command(self, ctx, name):
        """Get a command."""
        try:
            if sys.version_info[0] == 2:
                name = name.encode("ascii", "replace")
            mod = __import__("obsctl.commands.cmd_" + name, None, None, ["cli"])
        except ImportError:
            return None
        return mod.cli


# Click will use the ctx argument in the decorator.
# pylint: disable=unused-argument
@click.command(cls=CLI)
@PASS_CONTEXT
def cli(ctx):
    """An OBS command line interface."""
