# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module for interacting with _service files"""

from __future__ import print_function

import os
import sys

import lxml.etree as ET

import osc.core


class Service:
    """
    Interact with the _service file of the following format:

    <services>
      <service name="download_files">
        <param name="enforceupstream">yes</param>
      </service>
    </services>
    """

    def __init__(self, filename):
        """
        @param string filename the filename for the service
        """
        self.__filename = filename
        self.__services = None

    def init(self):
        """
        Load from disk the services file (or initialize it to empty otherwise).
        @return self
        """
        services = self.__read(self.__filename)

        if services is None:
            services = ET.fromstring("<services/>")

        self.__services = services

        return self

    def write(self):
        """
        Flush local changes to the services file to disk.
        @return self
        """
        with open(self.__filename, "w") as file:
            file.write(ET.tostring(self.__services, pretty_print=True))

        return self

    def merge(self, filename):
        """
        Merge the contents of the source file into this object.
        @param string sourceFilename the _source file with new service entries
        @return self
        """
        self.__merge(filename)

        return self

    @staticmethod
    def __read(filename):
        """
        @return object the XML object for services or None
        """
        if not os.path.isfile(filename):
            return None

        tree = ET.parse(filename, ET.XMLParser(remove_blank_text=True))

        services = tree.getroot()

        # If the XML is in an unexpected format, then bomb out
        assert services.tag == "services", "Root tree should begin with services"

        return services

    def __merge(self, filename):
        services = self.__read(filename)

        assert services is not None, "Source _service file does not exist"

        new_services = []
        for service in services:
            matches = lambda existing, srv=service: ET.tostring(srv) == ET.tostring(srv)

            missing = not filter(matches, self.__services)
            if missing:
                new_services.append(service)

        for new_service in new_services:
            self.__services.append(new_service)


def run_source_services(service_data, obspkgdir):
    """Run source services."""
    # Parse the service XML data
    try:
        service_xml = ET.fromstring(service_data)
    except ET.ParseError as error:
        # Confirmed this is the correct format in the docs:
        # https://docs.python.org/3/library/xml.etree.elementtree.html
        # pylint: disable=unpacking-non-sequence
        line, column = error.position
        print("XML error in source service data on line {}, column {}".format(line, column))
        sys.exit(10)

    # Execute the source services
    svc_info = osc.core.Serviceinfo()
    svc_info.read(service_xml)

    # Service must be run in project dir...
    curdir = os.getcwd()
    os.chdir(obspkgdir)
    svc_exec = svc_info.execute(obspkgdir)
    os.chdir(curdir)
    return svc_exec
