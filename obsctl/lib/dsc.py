# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module for interacting with Debian Source Control (DSC) files"""

import os
import re
import hashlib
import subprocess


class Dsc:
    """Interact with Debian Source Control (DSC) files"""

    SECTION_TO_HASH_FUNC = {
        "Checksums-Sha1:": hashlib.sha1,
        "Checksums-Sha256:": hashlib.sha256,
        "Files:": hashlib.md5,
    }

    def __init__(self, filename):
        """
        @param string filename the dsc filename
        """
        self.__filename = filename
        self.__dsc = None

    def init(self):
        """Initialize the object"""
        with open(self.__filename, "r") as handle:
            self.__dsc = handle.read()

        return self

    def write(self):
        """Write the dsc file"""
        with open(self.__filename, "w") as handle:
            handle.write(self.__dsc)

        return self

    # TODO: Sanity check that the version includes the suffix +dattoX

    def update_sha_checksums(self):
        """Update the sha checksums within the debian source control file."""
        self.__dsc = "\n".join(Dsc.__process_sha_lines(self.__dsc.split("\n")))

        return self

    def strip_gpg_headers(self):
        """Remove the GPG header from the debian source control file."""
        process = subprocess.Popen(
            ["gpg", "-o-", "-"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        process.stdin.write(self.__dsc)

        output, _ = process.communicate()

        # gpg is always returning an exit code of 2 for me, which means no error checking :-(
        # assert process.returncode == 0, 'Unable to strip GPG header'

        # because we can't check the exit code, if gpg fails, we'll just leave the __dsc file alone
        if output:
            self.__dsc = output

        return self

    @staticmethod
    def __process_sha_lines(input_lines):
        """Iterate over the lines within the dsc file and update the file shas and byte counts."""
        output_lines = []
        hash_function = None
        for line in input_lines:
            # e.g.: 1a546a1a2ed3a39d49ada99821db401a 1234878 hivex_1.3.9.orig.tar.gz
            match = re.search(r" (\w+) (\d+) (.*)", line)

            if not match or not hash_function:
                if line.strip() in Dsc.SECTION_TO_HASH_FUNC:
                    hash_function = Dsc.SECTION_TO_HASH_FUNC[line.strip()]
                else:
                    hash_function = None
            else:
                (_, _, filename) = match.group(1, 2, 3)

                # If the file isn't downloaded, we're not going to recalculate the hash
                if os.path.isfile(filename):
                    new_size = os.path.getsize(filename)
                    with open(filename, "rb") as source_file:
                        # It is actually callable by this point
                        # pylint: disable=not-callable
                        new_hash = hash_function(source_file.read()).hexdigest()
                    line = " %s %d %s" % (new_hash, new_size, filename)

            output_lines.append(line)
        return output_lines

    def get_debian_source_filename(self):
        """Get the filename of the .debian.tar.xz"""
        return Dsc.__find_debian_source_filename(self.__dsc.split("\n"))

    @staticmethod
    def __find_debian_source_filename(input_lines):
        for line in input_lines:
            match = re.search(r" \w+ \d+ (.*\.debian\..*)", line)

            if match:
                return match.group(1)

        return None
