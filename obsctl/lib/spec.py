# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module for manipulating spec file data"""

from __future__ import print_function

import datetime

import rpm


def __sanitize_label_for_version(label):
    # Strings in version fields must be sanitized to the intersection of allowable things in rpm and dpkg
    # That means no dashes, underscores, forward/back slashes, and colons
    # for note: rpm allows only the following separators: period (.), underscore (_), plus (+), and tilde (~)
    # for note: dpkg allows only the following separators: period (.), dash (-), plus (+), and tilde (~)
    # The intersection of the two means we can only use periods, pluses, and tildes
    # The current rules are as follows:
    # * underscore/dash -> eliminate
    # * colons -> convert to periods
    # * slashes -> convert to pluses
    # * Everything is converted to lowercase
    return (
        label.replace("-", "")
        .replace("_", "")
        .replace(":", ".")
        .replace("/", "+")
        .replace("\\", "+")
        .lower()
    )


def generate_snapshot_version(
    src_repo_reltag, src_repo_type, src_repo_rev, src_repo_branch, spec_rev_form
):
    """Generate a snapshot version, given a versioning form"""
    # Check for valid revision form value
    spec_rev_forms = ["postrel", "prerel", "prerel-upver"]

    if spec_rev_form not in spec_rev_forms:
        raise ValueError("Invalid snapshot version form type")

    # Validate the VCS type
    vcs_types = ("git", "hg", "svn")
    if src_repo_type not in vcs_types:
        raise ValueError("Invalid version control system type")

    # Generate a short hash for hg/git, reuse rev for subversion
    if src_repo_type in ("git", "hg"):
        repo_tag_shorthash = src_repo_rev[:7]
    else:
        repo_tag_shorthash = src_repo_rev

    # Generate the datetime stamp for the build
    pkg_build_datetime = "{:%Y%m%d%H%M}".format(datetime.datetime.now())

    # Configure version form
    # 0: Version, 1: VCS, 2: Build datetime stamp, 3: VCS revision, 4: sanitized branch name

    if spec_rev_form == "postrel":
        spec_ver_form = "{0}+{1}{2}.{3}.{4}"
    elif spec_rev_form == "prerel":
        spec_ver_form = "{0}~{1}{2}.{3}.{4}"
    elif spec_rev_form == "prerel-upver":
        spec_ver_form = "{0}.0.1~{1}{2}.{3}.{4}"
    else:
        spec_ver_form = "{0}.{1}{2}.{3}.{4}"

    return spec_ver_form.format(
        src_repo_reltag,
        src_repo_type,
        pkg_build_datetime,
        repo_tag_shorthash,
        __sanitize_label_for_version(src_repo_branch),
    )


def __get_source_property(src_spec, rpm_field_macro):
    # Retrieve the rpmspec object for the spec file
    rpmspec = rpm.spec(src_spec)
    # Query for the value of the macro
    return rpmspec.sourceHeader.format(rpm_field_macro)


def validate_version_match(src_spec, src_repo_reltag):
    """Validate that the version set in the spec matches the release tag"""
    # Retrieve the version from the source spec
    src_spec_version = __get_source_property(src_spec, "%{version}")
    return src_repo_reltag.strip() == src_spec_version.strip()
