# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Interact with OBS via OSC"""

from __future__ import print_function

import tempfile
import textwrap
import shutil
import pathlib
import glob

from xml.etree import ElementTree as ET

import osc.conf
import osc.core
import osc.oscerr

from osc.util.helper import decode_it

try:
    from osc.connection import http_GET
except ImportError:
    # Support pre-1.0 versions of osc
    from osc.core import http_GET


# TODO: Turn into class and make it context-aware


def configure(obs_server_addr, obs_auth_user, obs_auth_pass, insecure_connection=False):
    """Configure the Open Build Service Commander"""

    # SSL check setting
    obs_ssl_check = "1"
    if insecure_connection is True:
        obs_ssl_check = "0"

    # Open Build Service Commander configuration file data
    obs_server_config = textwrap.dedent(
        """\
    [general]
    apiurl = {server}
    do_package_tracking = 1


    [{server}]
    user = {user}
    pass = {password}
    keyring = 0
    sslcertck = {ssl_check}
    """
    ).format(
        server=obs_server_addr, user=obs_auth_user, password=obs_auth_pass, ssl_check=obs_ssl_check
    )

    # Make a temporary config file to set up for using with osc and use it
    oscconf = tempfile.NamedTemporaryFile(suffix=".oscrc")
    oscconf.write(obs_server_config.encode("utf-8"))
    oscconf.flush()
    osc.conf.get_config(override_conffile=oscconf.name)

    return oscconf


def checkout_obs_package(
    obs_server_addr,
    obs_project,
    obs_package,
    obs_auth_user,
    src_repo_owner,
    src_repo_name,
    create_obs_pkg_if_none=False,
):
    # pylint: disable=too-many-arguments
    """Checkout a package from OBS"""

    obs_newpkg_config = """
    <package name="{1}" project="{0}">
      <title>{1} for {0}</title>
      <description>{1} for {0} created by {2}. Maintained at {3}/{4}</description>
    </package>
    """.format(
        obs_project, obs_package, obs_auth_user, src_repo_owner, src_repo_name
    )

    # Check to see if the package space exists on OBS
    osc_prj_packagelist = list(osc.core.meta_get_packagelist(obs_server_addr, prj=obs_project))

    if obs_package in osc_prj_packagelist:
        print("Package is configured remotely, proceeding to checkout...")
    else:
        # If it doesn't exist and we are allowing arbitrary package
        # space creation, create the package space and proceed onward
        if create_obs_pkg_if_none is True:
            print("Package is not configured remotely, constructing package!")
            osc.core.edit_meta(
                metatype="pkg",
                data=obs_newpkg_config,
                edit=False,
                apiurl=obs_server_addr,
                path_args=(obs_project, obs_package),
            )
            print("Package space created, continuing to checkout...")
        else:
            print("Package is not configured remotely!")
            return False

    # Create directory to check out package sources
    pkg_checkout_dir = tempfile.mkdtemp(suffix="{0}-{1}".format(obs_project, obs_package))

    # Check out the package sources
    osc.core.checkout_package(
        apiurl=obs_server_addr, project=obs_project, package=obs_package, outdir=pkg_checkout_dir
    )

    # Return osc Package object
    return osc.core.Package(workingdir=pkg_checkout_dir)


def update_files_for_obs_package(
    obs_working_pkg, obs_package_ver, sources, service_file, keep_existing_files=False
):
    """Update files for a package in OBS (including a _service file)"""
    # Append service file to sources list
    sources.append(service_file)
    # Call to the regular method with the service file appended to the list
    update_sources_for_obs_package(obs_working_pkg, obs_package_ver, sources, keep_existing_files)


def update_sources_for_obs_package(
    obs_working_pkg, obs_package_ver, sources, keep_existing_files=False
):
    """Update sources for a package in OBS"""
    if not keep_existing_files:
        # Purge all sources and the _service file
        prjfiles = glob.glob("{0}/*".format(obs_working_pkg.absdir))

        for prjfile in prjfiles:
            obs_working_pkg.delete_file(pathlib.PurePath(prjfile).name)

    # Send changes to OBS

    # Copy files to OBS package space
    for source in sources:
        if source.endswith("_obssrcservice"):
            # Copy the file as "_service"
            shutil.copy2(source, "{0}/_service".format(obs_working_pkg.absdir))
            # Remove the original file from sources
            sources.remove(source)
            # Add the "_service" file to the sources list
            sources.append("{0}/_service".format(obs_working_pkg.absdir))
        else:
            shutil.copy2(source, obs_working_pkg.absdir)

    # Add files
    for source in sources:
        try:
            obs_working_pkg.addfile(pathlib.PurePath(source).name)
        except osc.oscerr.PackageFileConflict:
            print("Already tracked source {}... continuing!".format(pathlib.PurePath(source).name))
            # We're explicitly noting that we're ignoring this error
            # pylint: disable=unnecessary-pass
            pass

    # Commit changes
    obs_working_pkg.commit(msg="Committed {0}-{1}".format(obs_working_pkg.name, obs_package_ver))


def get_project_repos(obs_server_addr, obs_project):
    """Get an OBS project's repositories as a list of Repo objects"""
    return list(osc.core.get_repos_of_project(obs_server_addr, obs_project))


def get_package_results(obs_server_addr, obs_project, obs_package, repo=None, arch=None):
    """Get the build results for a package"""
    return [
        res
        for result in osc.core.get_package_results(obs_server_addr, obs_project, obs_package)
        for res, is_multi in osc.core.result_xml_to_dicts(result)
        if (repo and arch and res["repo"] == repo and res["arch"] == arch)
        or (not repo and not arch)
    ]


def get_package_result_state(obs_server_addr, obs_project, obs_package, repo, arch):
    """Get a package's build state for a specific distro and arch"""
    return get_package_results(obs_server_addr, obs_project, obs_package, repo, arch)[0]["state"]


def get_build_job_status(obs_server_addr, obs_project, obs_package, repo, arch):
    """Get a package's build job status for a specific distro and arch"""
    url = osc.core.makeurl(
        obs_server_addr, ["build", obs_project, repo, arch, obs_package, "_jobstatus"]
    )
    response = http_GET(url)
    return ET.parse(response)


def get_package_build_jobid(obs_server_addr, obs_project, obs_package, repo, arch):
    """Get a package's build job ID for a specific distro and arch"""
    jobid_result = get_build_job_status(obs_server_addr, obs_project, obs_package, repo, arch).find(
        "jobid"
    )
    return jobid_result.text if hasattr(jobid_result, "text") else None


def print_buildlog(obs_server_addr, obs_project, obs_package, repo, arch):
    """Print a package's build log for a specific distro and arch in real-time"""
    osc.core.print_buildlog(obs_server_addr, obs_project, obs_package, repo, arch)


def get_log_snippet(obs_server_addr, obs_project, obs_package, repo, arch, length):
    # pylint: disable=too-many-arguments
    """Get a snippet of a package's build log for a specific distro and arch"""
    query = {"nostream": 1, "end": f"{length}"}
    url = osc.core.makeurl(
        obs_server_addr, ["build", obs_project, repo, arch, obs_package, "_log"], query=query
    )
    response = http_GET(url)
    return decode_it(response.read())
