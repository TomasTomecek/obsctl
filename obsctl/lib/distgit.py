# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module for methods for accessing Dist-Git resources"""

import re

from urlgrabber.grabber import URLGrabber


def download_sources(package_name, distgit_repo_checkout, distribution):
    """Download sources stored in a given distributions's Dist-Git system"""
    # Check for valid distribution sources
    distributions = ["fedora"]

    if distribution.lower() not in distributions:
        raise ValueError("Invalid distribution source")

    # Set the appropriate baseurl for the lookaside cache
    if distribution == "fedora":
        lookaside_baseurl = "https://src.fedoraproject.org/repo/pkgs"
        sources = parse_sources_file(
            package_name, "{}/sources".format(distgit_repo_checkout), lookaside_baseurl
        )

    # Download each file from the lookaside cache
    for source_filename, source_url in sources.items():
        filegrabber = URLGrabber(reget="simple")
        filegrabber.urlgrab(source_url, "{}/{}".format(distgit_repo_checkout, source_filename))


def parse_sources_file(package_name, sources_filepath, lookaside_baseurl):
    """Parse the Dist-Git 'sources' file and return dictionary of URLs to download"""
    # Set up sources dictionary
    sources = {}

    # Read in the sources file
    sources_file = open(sources_filepath, "r")

    for source in sources_file.readlines():
        fields = source.split()
        source_baseurl = "{}/{}/".format(lookaside_baseurl, package_name)
        # Check to see if it's legacy MD5 or new-style with arbitrary hash types
        if len(fields) == 2:
            # This is legacy MD5 sources line, construct the old URL style
            source_md5sum, source_filename = fields
            source_url = "{baseurl}/{filename}/{md5sum}/{filename}".format(
                baseurl=source_baseurl, filename=source_filename, md5sum=source_md5sum
            )
            sources[source_filename] = source_url
        else:
            # This is the new-style sources line, parse the file accordingly
            sources_file_pattern = re.compile(r"^(.*) \((.*)\) = (.*)")
            pattern_match = sources_file_pattern.match(source)
            source_hashtype = pattern_match.group(1)
            source_filename = pattern_match.group(2)
            source_checksum = pattern_match.group(3)
            source_url = "{baseurl}/{filename}/{hashtype}/{checksum}/{filename}".format(
                baseurl=source_baseurl,
                filename=source_filename,
                hashtype=source_hashtype.lower(),
                checksum=source_checksum,
            )
            sources[source_filename] = source_url

    sources_file.close()
    return sources
