# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


"""Module for methods for operating on tarballs"""

import shutil
import tarfile
import tempfile
import subprocess


def rebuild_tarball(src_tarball, src_name, version):
    """Rebuild a tarball with a versioned top level directory"""
    # Pull out original sources to tarfix_dir
    tarfix_dir = tempfile.mkdtemp(suffix="tarfix")
    extract_tarball(src_tarball, tarfix_dir)

    # Generate new tarball working area
    new_tgz_dir = tempfile.mkdtemp(suffix="newtgz")

    # Make new tarball name
    rebuilt_tarball_prefix = "{0}-{1}".format(src_name, version)

    # Move the files to the new working area
    shutil.move(
        "{0}/{1}".format(tarfix_dir, src_name), "{0}/{1}-{2}".format(tarfix_dir, src_name, version)
    )

    # Generate the new tarball
    new_tarball = shutil.make_archive(
        base_name="{0}/{1}".format(new_tgz_dir, rebuilt_tarball_prefix),
        format="gztar",
        root_dir=tarfix_dir,
        base_dir="./{0}-{1}".format(src_name, version),
    )
    # Purge the old working tree
    shutil.rmtree(tarfix_dir)

    return new_tarball


def extract_tarball(src_tarball, extract_path):
    """Extract a tarball to a given directory path"""
    orig_tarball = tarfile.open(name=src_tarball, mode="r:gz")
    orig_tarball.extractall(path=extract_path)
    orig_tarball.close()


# TODO: Redo using Python functions
def create_debian_tarball(filename):
    """Create a tarball of the debian/ folder"""
    # Determine the compression flag based on the filename
    compression_flag = ""
    if filename.endswith(".xz"):
        compression_flag = "J"
    if filename.endswith(".gz"):
        compression_flag = "z"
    if filename.endswith(".bz2"):
        compression_flag = "j"

    process = subprocess.Popen(["tar", "c{0}f".format(compression_flag), filename, "debian"])
    process.communicate()
    assert process.returncode == 0, "Unable to create debian tarball"
