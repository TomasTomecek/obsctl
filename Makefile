# Location on where to build packages
BUILDDIR := $(CURDIR)/pkgbuild

# Flags to pass to debbuild/rpmbuild
PKGBUILDFLAGS := --define "_topdir $(BUILDDIR)" -ba --with devmode

# Command to create the build directory structure
PKGBUILDROOT_CREATE_CMD = mkdir -p $(BUILDDIR)/DEBS $(BUILDDIR)/SDEBS $(BUILDDIR)/RPMS $(BUILDDIR)/SRPMS \
			$(BUILDDIR)/SOURCES $(BUILDDIR)/SPECS $(BUILDDIR)/BUILD $(BUILDDIR)/BUILDROOT

PYTHON := $(shell which python3)
DESTDIR ?= /

all: help

help:
	@echo ""
	@echo "Break dance, not hearts!"
	@echo "------------------------"
	@echo ""
	@echo ""
	@echo "Targets"
	@echo "-------"
	@echo "make build        - Build the code"
	@echo "make install      - Install the code to the system"
	@echo "make style        - Validate the code style against black"
	@echo "make lint         - Validate the code against pylint"
	@echo "make test         - Run unit tests"
	@echo "make dev-check    - Run code validators and tests for development"
	@echo "make watch        - Run tests in a loop triggered on file edits"
	@echo "make venv         - Create a Python virtual environment"
	@echo "make deb          - Create Debian package"
	@echo "make rpm          - Create RPM package"
	@echo "make clean        - Delete build directory"
	@echo ""


build:
	$(PYTHON) setup.py build

install: build
	$(PYTHON) setup.py install -O1 --skip-build --root=$(DESTDIR)

.PHONY: style
style:
	if [ -e ./venv ]; then source ./venv/bin/activate; fi && \
	$(PYTHON) -m black --check --diff ./obsctl ./tests ./setup.py

.PHONY: lint
lint:
	if [ -e ./venv ]; then source ./venv/bin/activate; fi && \
	$(PYTHON) -m pylint ./obsctl ./tests --rcfile=.pylintrc

.PHONY: test
test:
	if [ -e ./venv ]; then source ./venv/bin/activate; fi && \
	$(PYTHON) -m pytest --cov=obsctl --cov-report term --cov-report html:cov_html tests/

.PHONY: dev-check
dev-check: style lint test

.PHONY: watch
watch:
	while true; do find obsctl/ tests/ | entr -d make dev-check; done

.PHONY: venv
venv: clean
	$(PYTHON) -m venv --system-site-packages ./venv && source ./venv/bin/activate && pip install -c ./constraints-test.txt --editable .[lint,test]

check: test

dev-environment: venv

.PHONY: clean
clean:
	rm -rf pkgbuild venv obsctl.egg-info
	find . -name '*.py[co]' -o -name __pycache__  -exec rm -rf {} \;

.PHONY: pkgprep
pkgprep: clean
	$(PKGBUILDROOT_CREATE_CMD)
	tar --exclude=./pkgbuild --exclude=.git --transform 's,^\.,obsctl,' -czf $(BUILDDIR)/SOURCES/obsctl.tar.gz .
	cp dist/obsctl.spec $(BUILDDIR)/SPECS/obsctl.spec

.PHONY: deb
deb: pkgprep
	debbuild $(PKGBUILDFLAGS) $(BUILDDIR)/SPECS/obsctl.spec

.PHONY: rpm
rpm: pkgprep
	rpmbuild $(PKGBUILDFLAGS) $(BUILDDIR)/SPECS/obsctl.spec
