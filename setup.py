# This file is part of obsctl.
#
# Copyright Datto, Inc.
#
# Licensed under the GNU General Public License Version 2
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later
#
# obsctl is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# obsctl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with obsctl.  If not, see <https://www.gnu.org/licenses/>.


from setuptools import setup

setup(
    name="obsctl",
    version="0.7.0",
    license="GPLv2+",
    description="Unified high level interface for common actions with the Open Build Service",
    keywords="openbuildservice obsctl osc packaging",
    # From https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # Development status
        "Development Status :: 4 - Beta",
        # Target audience
        "Intended Audience :: Developers",
        # Type of software
        "Topic :: Software Development :: Build Tools",
        "Topic :: System :: Archiving :: Packaging",
        # Kind of software
        "Environment :: Console",
        # License (must match license field)
        "License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)",
        # Operating systems supported
        "Operating System :: POSIX :: Linux",
        # Supported Python versions
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    url="https://gitlab.com/datto/engineering/DevOps/obsctl",
    author="Neal Gompa, Mark Bluemer",
    author_email="ngompa@datto.com, mbluemer@datto.com",
    maintainer="Neal Gompa",
    maintainer_email="ngompa@datto.com",
    packages=["obsctl", "obsctl.commands", "obsctl.lib"],
    include_package_data=True,
    extras_require={"lint": ["black", "pylint"], "test": ["pytest", "pytest-cov"]},
    install_requires=["click", "osc", "setuptools", "lxml", "rpm", "urlgrabber"],
    python_requires=">=3.6",
    entry_points="""
        [console_scripts]
        obsctl=obsctl.cli:cli
    """,
)
