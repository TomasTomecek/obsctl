# obsctl

[![build result](https://build.opensuse.org/projects/isv:Datto:Utilities:OBSCtl:Mainline/packages/obsctl-git/badge.svg?type=default)](https://build.opensuse.org/package/show/isv:Datto:Utilities:OBSCtl:Mainline/obsctl-git)

`obsctl` is a unified high level interface for common actions with [Open Build Service](https://openbuildservice.org/) systems.

First and foremost, it is designed as a tool to simplify doing basic actions for automation purposes,
including continuous integration and delivery schemes.

It is inspired by Fedora's [`fedpkg`](https://pagure.io/fedpkg) tool, which provides a comprehensive
interface to working with various parts of Fedora's software delivery infrastructure, as well as enabling
CI and local development of packages, modules, and containers.

It is the hope that `obsctl` will eventually evolve to provide a similar interface for those who use
Open Build Service instances in the same way that [`rpkg`](https://docs.pagure.org/rpkg/index.html)
and `fedpkg` do for [Koji](https://koji.build) instances.

If you want to learn more about how Datto uses `obsctl` and the Open Build Service, [check out our blog post](https://datto.engineering/post/flexible-and-fast-software-delivery-with-the-open-build-service)
on the subject.

## Prerequisites

`obsctl` is an overlay on top of `osc`, the openSUSE (Build) Commander. Thus, you need `osc` installed.

Note that `osc` only available as a system package, so you will need to install it from your distribution's
package manager.

Similarly, `obsctl` depends on the Python bindings for RPM, which is also only available as a system package.

This is usually available in your distribution as `python3-rpm`/`rpm-python3` (for Python 3).
Please install it from your distribution package manager.

For some functionality to work correctly (e.g. scratch builds), it's highly recommended to use an RPM-based
Linux distribution, such as Fedora or openSUSE.

In addition, `obsctl` has a few other Python dependencies:

* `click`
* `setuptools`
* `lxml`
* `pathlib` (when using Python < 3.4)

On Fedora, the following will install everything you need to run `obsctl` for your current system:

```bash
$ sudo dnf builddep dist/obsctl.spec
```

## Development

The preferred way to do development is to leverage [Podman](https://podman.io/)/[Docker](https://docs.docker.com/install/)
to build and test as it would be used.

### Podman/Docker

The instructions below will use the `podman` command, but you can easily substitute for `docker`
and it should easily just work the same.

First, pull down the container of the target environment.

For Python 3 development, Fedora 30 or newer is recommended currently (as `osc` is Python 3 there).

For the instructions below, we'll work with Fedora 30.

```bash
$ sudo podman pull fedora:30
```

Assuming you're in the root directory of the Git checkout of `obsctl`, set up the container:

```bash
$ sudo podman run --privileged --rm -it -v $PWD:/code fedora:30 /bin/bash
```

Once in the container environment, set up your build and development environment:

```bash
# Change to the code location
[]$ cd /code
# Install build and test dependencies
[]$ dnf --assumeyes install make rpm-build "dnf-command(builddep)" "python3dist(black)" "python3dist(pylint)" "python3dist(pytest)" "python3dist(pytest-cov)"
# Install build dependencies
[]$ dnf --assumeyes builddep ./dist/obsctl.spec
```

You're now set to do work on the code!

#### Testing the code

There are two ways to test the changes you've made. The preferred way is by building and installing the RPM.
Alternatively, you can set up a Python virtual environment that will reuse system dependencies.

Finally, you will need to populate `/etc/obsctl/obsauth.json`, which you can do so by copying the
`obsauth.json.dist` file into the target location and editing accordingly.

There are a number of tests used against the code during development, and they can be run through
various targets in the Makefile.

```bash
# Run the code format check
[]$ make style
# Run the code lint check
[]$ make lint
# Run the unit test check
[]$ make test
```

These can assist in development.

##### Building and installing a development RPM

```bash
# Make the rpm
[]$ make rpm
# Install the rpm
[]$ dnf install ./pkgbuild/RPMS/noarch/obsctl*.rpm
```

##### Building and testing via a Python virtual environment

For Python 3, it's just as simple as running the command below:

```bash
[]$ make venv
```

The above approach will also make `obsctl` available to you, but you will need to install `osc` and the
required OBS source services by hand, like so:

```bash
[]$ dnf --assumeyes install osc obs-service-download_files obs-service-extract_file obs-service-set_version
```
